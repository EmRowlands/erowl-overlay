# Copyright Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=(python3_{11,12})
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 optfeature

if [ "${PV}" = 9999 ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/psy0rz/${PN}.git"
else
	MY_PV="${PV/_rc/-rc}"
	MY_P="${P/_rc/-rc}"

	MY_PV="${MY_PV/_beta/-beta}"
	MY_P="${P/_beta/-beta}"

	S="${WORKDIR}/${MY_P}"

	SRC_URI="https://github.com/psy0rz/${PN}/archive/v${MY_PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="Periodicly backup ZFS filesystems to other locations."
HOMEPAGE="https://github.com/psy0rz/zfs_autobackup"

LICENSE="GPL-3"
SLOT="0"

IUSE="test"

# Tests require root
RESTRICT="mirror !test? ( test )"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

# Note that this does not require sys-fs/zfs[python]
COMMON_DEP="
	sys-fs/zfs
$(python_gen_cond_dep '
	dev-python/colorama[${PYTHON_USEDEP}]
')"

RDEPEND="${COMMON_DEP}
	${PYTHON_DEPS}"

BDEPEND="test? (
	${RDEPEND}
$(python_gen_cond_dep '
	dev-python/pytest[${PYTHON_USEDEP}]
') )"

distutils_enable_tests pytest

PATCHES=(
	"${FILESDIR}/0001-Fix-test-imports.patch"
)

pkg_postinst() {
	optfeature "Support for rate limiting and buffering" sys-block/mbuffer
}
