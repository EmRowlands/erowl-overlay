# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{11..12} )

inherit distutils-r1 pypi

DESCRIPTION="Python API client library for Netbox."
HOMEPAGE="
	https://pypi.org/project/pynetbox/
	https://github.com/netbox-community/pynetbox
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

REQUIRED_USE="${PYTHON_REQUIED_USE}"

RDEPEND="
	${PYTHON_DEPS}
	<dev-python/requests-3[${PYTHON_USEDEP}]
"
BDEPEND="
	test? (
		dev-python/pytest[${PYTHON_USEDEP}]
		dev-python/pyyaml[${PYTHON_USEDEP}]
	)
"

EPYTEST_IGNORE=(
	tests/integration
)
distutils_enable_tests pytest
