# Copyright 2020-2022 Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A simple tool to synchronise git repositories"
HOMEPAGE="https://gitlab.com/EmRowlands/git-autosync"

LICENSE="AGPL-3"
SLOT="0"

RDEPEND="dev-vcs/git"

RESTRICT="mirror"

if [ "${PV}" = 9999 ]; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/EmRowlands/${PN}.git"
else
	KEYWORDS="amd64"
	SRC_URI="https://gitlab.com/EmRowlands/${PN}/-/archive/${PV}/${P}.tar.bz2"
fi

src_install() {
	dobin "${PN}"

	insinto /etc
	insopts -m0600
	newins example-config "${PN}"

	diropts -m0700
	keepdir "/var/lib/${PN}"
}
