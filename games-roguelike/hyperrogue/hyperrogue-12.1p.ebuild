# Copyright 2023 Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit edo flag-o-matic toolchain-funcs

SOURCE_REF="da687e8874ee969be373b589ac168120633c4533"
DESCRIPTION="A SDL roguelike in a non-euclidean world"
HOMEPAGE="https://roguetemple.com/z/hyper/"
SRC_URI="https://github.com/zenorogue/${PN}/archive/${SOURCE_REF:-v${PV}}.tar.gz -> ${P}.tar.gz"

S="${WORKDIR}/${PN}-${SOURCE_REF:-${PV}}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

IUSE="+glew +png +fontconfig"

DEPEND="
	media-libs/libglvnd
	media-libs/libsdl
	media-libs/sdl-gfx:=
	media-libs/sdl-mixer
	media-libs/sdl-ttf
	sys-libs/zlib
	media-fonts/dejavu
	fontconfig? ( media-libs/fontconfig )
	glew? ( media-libs/glew:= )
	png? ( media-libs/libpng:= )
"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_prepare() {
	edo sed -i Makefile -e "s:pkg-config:$(tc-getPKG_CONFIG):g"

	local objs=(
		$(sed -nE -e 's/#include "([^/]+).cpp"/\1/p' < hyper.cpp | tr -d '\r')
	)

	last_obj=hyper
	for obj in "${objs[@]}"; do
		edo sed -i Makefile -e "/^hyper_OBJS +\?= ${last_obj}\$(OBJ_EXTENSION)/a\
hyper_OBJS += ${obj}\$(OBJ_EXTENSION)"
		edo sed -i hyper.cpp -e "/#include \"${obj}\.cpp\"/d"
		last_obj="${obj}"
	done
	default
}

src_compile() {
	tc-export CXX

	append-cxxflags "-I ${ESYSROOT}/usr/include/SDL" '-DHYPERPATH=\"/usr/share/${PN}/\"'

	local myemakeargs=(
		HYPERROGUE_USE_GLEW="$(usex glew 1 0)"
		HYPERROGUE_USE_PNG="$(usex png 1 0)"
		FONTCONFIG="$(usex fontconfig 1 0)"
	)

	tc-env_build emake "${myemakeargs[@]}" makeh langen

	emake "${myemakeargs[@]}" autohdr.h
	emake "${myemakeargs[@]}"
}

src_install() {
	dobin "${PN}"

	insinto "/usr/share/${PN}"
	doins "${PN}-music.txt"
	doins -r music/

	einstalldocs
	dodoc CITATION.cff
}
