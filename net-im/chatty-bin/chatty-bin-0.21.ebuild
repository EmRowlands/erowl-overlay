# Copyright 1999-2022 Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

#
# This is a precompiled ebuild because gradle is not currently integrated with
# portage yet.
#
# https://wiki.gentoo.org/wiki/Gentoo_Java_Packing_Policy#Gradle
#

EAPI=8

inherit wrapper

DESCRIPTION="A chat software specifically made for Twitch"
HOMEPAGE="https://chatty.github.io/"
SRC_URI="https://github.com/chatty/chatty/releases/download/v${PV}/Chatty_${PV}.zip"

S="${WORKDIR}"

LICENSE="GPL-3+"
SLOT="0"

KEYWORDS="amd64"

RDEPEND=">=virtual/jre-1.8"
BDEPEND="app-arch/unzip"

RESTRICT="mirror"

src_install() {
	insinto /opt/${PN}/lib/
	doins Chatty.jar
	doins -r sounds img
	dodoc readme.txt
	make_wrapper chatty "java -jar ${EPREFIX}/opt/${PN}/lib/Chatty.jar"
}
