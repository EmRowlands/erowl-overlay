# Copyright 1999-2021 Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_APPIMAGE="Ripcord-${PV}-x86_64.AppImage"
MY_PN="${PN/-bin/}"

inherit desktop edo xdg-utils

DESCRIPTION="a desktop chat client for group-centric services like Slack and Discord"
HOMEPAGE="https://cancel.fm/ripcord/"
SRC_URI="https://cancel.fm/dl/${MY_APPIMAGE}"

S="${WORKDIR}/squashfs-root"

QA_PRESTRIPPED="/opt/ripcord/${MY_APPIMAGE}"

LICENSE="ripcord-alpha-preview"
SLOT="0"
KEYWORDS="-* ~amd64"
IUSE="+pulseaudio"

RESTRICT="mirror bindist"

RDEPEND="dev-libs/glib:2
	dev-libs/libbsd
	dev-libs/libpcre
	sys-fs/fuse
	sys-libs/zlib
	virtual/opengl
	x11-libs/libX11
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXdmcp
	!pulseaudio? ( media-sound/apulse )"

BDEPEND="sys-fs/fuse"

src_unpack() {
	edo cp "${DISTDIR}/${A}" "${WORKDIR}"
	cd "${WORKDIR}" || die
	edo chmod +x "${MY_APPIMAGE}"
	edo "./${MY_APPIMAGE}" --appimage-extract
	edo mv "${MY_APPIMAGE}" "${S}"
}

src_prepare() {
	default
	edo sed -i \
		-e "/^Exec=/s/Ripcord/ripcord/" \
		Ripcord.desktop
	if ! use pulseaudio; then
		edo sed -i \
			-e "/^Exec=/s/ripcord/apulse ripcord/" \
			Ripcord.desktop
	fi
}

src_install() {
	doicon Ripcord_Icon.png
	domenu Ripcord.desktop

	insinto "/opt/${MY_PN}"
	doins "${MY_APPIMAGE}"
	fperms +x "/opt/${MY_PN}/${MY_APPIMAGE}"
	dosym -r "/opt/${MY_PN}/${MY_APPIMAGE}" "/usr/bin/${MY_PN}"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
	xdg_icon_cache_update
}
