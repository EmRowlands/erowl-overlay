# Copyright 2020-2022 Emily Rowlands
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=(python3_{11,12})
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 bash-completion-r1 optfeature

MY_PV="${PV/_beta/b}"
MY_P="${P/_beta/b}"

DESCRIPTION="Keep track of your favorite playlists on YouTube and many other places"
HOMEPAGE="https://github.com/woefe/ytcc"

S="${WORKDIR}/${MY_P}"

if [ "${PV}" = 9999 ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/woefe/ytcc"
else
	SRC_URI="https://github.com/woefe/ytcc/archive/v${MY_PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64"
fi

LICENSE="GPL-3+"
SLOT="0"

IUSE="test network-tests"

RESTRICT="mirror !test? ( test )"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

COMMON_DEP="$(python_gen_cond_dep '
	net-misc/yt-dlp[${PYTHON_USEDEP}]
	dev-python/click[${PYTHON_USEDEP}]
	dev-python/wcwidth[${PYTHON_USEDEP}]
		')"

RDEPEND="${COMMON_DEP}
	${PYTHON_DEPS}"

distutils_enable_tests pytest

BDEPEND="
	test? ( ${COMMON_DEP}
	${PYTHON_DEPS}
	$(python_gen_cond_dep '
		dev-python/pytest[${PYTHON_USEDEP}]
	') )"

src_compile() {
	# Regenerate man page if using git
	if [ "${PV}" = 9999 ]; then
		python3 scripts/make_doc.py
	fi
}

src_install() {
	python_domodule ytcc
	python_newscript ytcc.py ytcc

	doman doc/ytcc.1

	newbin scripts/ytccf.sh ytccf

	newbashcomp "scripts/completions/bash/${PN}.completion.sh" "${PN}"

	insinto /usr/share/zsh/site-functions
	doins "scripts/completions/zsh/_${PN}"

	insinto /usr/share/fish/vendor_completions.d
	doins "scripts/completions/fish/${PN}.fish" "${PN}"
}

python_test() {
	mkdir "${T}/path"
	cat > "${T}/path/mpv" <<EOF
#!/bin/sh
exit 0
EOF
	chmod +x "${T}/path/mpv"
	export PATH="${T}/path:${PATH}"

	local mypytestargs=( )
	if ! use network-tests; then
		mypytestargs+=(
			--deselect 'test/test_cli.py::test_subscribe'
			--deselect 'test/test_cli.py::test_update'
			--deselect 'test/test_cli.py::test_download'
			--deselect 'test/test_cli.py::test_import'
			--deselect 'test/test_cli.py::test_import_duplicate'
		)
	fi
	epytest "${mypytestargs[@]}"
}

pkg_postinst() {
	optfeature "an alternate frontend" app-shells/fzf
	optfeature "displaying thumbnails in alternate frontend" x11-terms/kitty
	optfeature "playing videos in an external player" media-video/mpv
	if [ "${PV}" = "9999" ]; then
		if [ "${REPLACING_VERSIONS}" != "9999" ]; then
			ewarn "There may be schema incompatibilites introduced at any time."
			ewarn "The schema update takes place when ${PN} is first run after an upgrade."
			ewarn "You may wish to make a backup of your database, which is by default ~/.config/ytcc/ytcc.db"
			ewarn "This message will only be shown once."
		fi
	else
		versions=""
		# Add new schema updates at the front of this
		for version in 2.2.0 2.1.0; do
			if ver_test "${REPLACING_VERSIONS}" -lt "${version}"; then
				versions="${version}, ${versions}"
			fi
		done
		if [ -n "${versions}" ]; then
			ewarn "The following versions introduced a non-backwards compatible schema update:"
			ewarn "${versions%, }"
			ewarn "The schema update takes place when ${PN} is first run after an upgrade."
			ewarn "You may wish to make a backup of your database, which is by default ~/.config/ytcc/ytcc.db"
			ewarn "This message will be shown after every schema update."
		fi
	fi
}
